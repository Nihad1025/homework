function newUser() {
    let name;
    while (!name) {
        name = prompt("Enter Name")
    }
    let surname;
    while (!surname) {
        surname = prompt("Enter SurName")
    }

    let user = {
        firstName: name,
        lastName: surname,

        getLogin: function() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        }

    }
    return user;
}

const user = newUser();
console.log(user.getLogin());