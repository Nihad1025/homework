1)A function is a block of organized, reusable code that is used to perform a single, related action. 
Functions provide better modularity for your application and a high degree of code reusing.
Good thing about functions is that they are famous with several names. Different programming languages name them differently, for example, functions, methods, sub-routines, procedures, etc. 
If you come across any such terminology, then just imagine about the same concept, which we are going to discuss in this tutorial.

function myFunction(p1, p2) {
  return p1 * p2;   // The function returns the product of p1 and p2
}

Defining a Function

Return Type − A function may return a value. The return_type is the data type of the value the function returns. 
Some functions perform the desired operations without returning a value. In this case, the return_type is the keyword void.

Function Name − This is the actual name of the function. The function name and the parameter list together constitute the function signature.

Parameter List − A parameter is like a placeholder. When a function is invoked, you pass a value as a parameter. 
This value is referred to as the actual parameter or argument. The parameter list refers to the type, order, and number of the parameters of a function. 
Parameters are optional; that is, a function may contain no parameters.

Function Body − The function body contains a collection of statements that defines what the function does.
Why Functions?
We can reuse code: Define the code once, and use it many times.

2)The arguments object is a special construct available inside all function calls. It represents the list of arguments that were passed in when invoking the function. 
Since JavaScript allows functions to be called with any number args, we need a way to dynamically discover and access them.

const myfunc = function(one) {
  arguments[0] === one;
  arguments[1] === 2;
  arguments.length === 3;
}

myfunc(1, 2, 3);

The argument values passed in and out of a call to a function are termed the actual arguments. Instructions in the function code block access the actual argument values using the formal arguments of the function definition.

Actual argument definitions are variable definitions in an arg block that specify the arg segment. Formal argument definitions are variable definitions in the function header that specify the arg segment. 
Variable declaration and definitions that specify the arg segment cannot appear in any other place. See Arg Block and Function.

A function specifies a list of zero or more output formal arguments and a list of zero or more input formal arguments. A call instruction provides a corresponding list of zero or more output actual arguments and zero or more input actual arguments.